# No 1. Rancang produk digital berbasis pemrosesan video deskripsi dan list use case dari produk tersebut !
Produk Digital yang di buat adalah image stabilizer dimana ketika video yang memiliki fluktuasi frame yang tidak semestinya, maka akan langsung di isi oleh frame yang baru sehingga video menjadi lebih smooths
# No 2. Mampu menjelaskan cara kerja image processing dan image compression pada aplikasi yang dibuat.
```mermaid
flowchart TD
A[Start] --> B[Masukkan nama file video]
B --> C[Buka video]
C --> D[Mendapatkan properti video]
D --> E[Inisialisasi variabel]
E --> F[Inisialisasi VideoWriter]
F --> G[Loop: Baca frame]
G --> H[Ubah frame menjadi grayscale]
H --> I[Proses stabilisasi pada frame pertama]
I --> J[Hitung optical flow]
J --> K[Hitung pergeseran rata-rata]
K --> L[Terapkan moving average]
L --> M[Buat matriks transformasi]
M --> N[Stabilisasi frame]
N --> O[Tambahkan frame stabil ke video hasil]
O --> P[Tampilkan frame asli dan hasil stabilisasi]
P --> Q[Tulis frame hasil stabilisasi ke video]
Q --> R[Tombol 'q' untuk keluar]
R --> G
G --> S[Tutup video dan jendela tampilan]
S --> T[Tutup video hasil]
T --> U[End]
```
# No 3. Mampu menjelaskan dan mendemonstrasikan aspek kecerdasan buatan dalam produk image processing yang dibuat
 aspek kecerdasan buatan pada code ini dapat di lihat dari library yang digunakan terutama CV2 yang di gunakan untuk image processing haarcascade ini yang di sediakan dengan dataset nya juga digunakan untuk mengolah image yang dimana pada akhirnya si camera dapat membaca dan mendeteksi apakah ada wajah atau tidak dalam hasil tangkapannya
![link folder](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/tree/main/face%20detection)

