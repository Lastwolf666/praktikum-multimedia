# Praktikum Multimedia

## Untuk Uas sudah di upload pada folder Video Stabilizer dan AI Voice Sovits
## Untuk UTS di upload pada folder face detection dan Audio Translasi
# No 1. Mampu mendemonstrasikan penggunaan teknologi image processing dan image compression dalam bentuk aplikasi sederhana. 
![demonstrasi image processing](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/raw/main/Image_processing.gif).
![demonstrasi image](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/raw/main/image_compression.gif)
# No 2. Mampu menjelaskan cara kerja image processing dan image compression pada aplikasi yang dibuat.
```mermaid
flowchart TD
A[import library] --> B[pathlib]
A --> C[cv2]
A --> D[PIL]
A --> E[datetime]
A --> F[os]
B-->G(mengambil dataset haarcascade)
C-->H(Klasifikasi Cascade)
C-->I(camera read)
I-->J(Frame)
J-->K(Konversi ke Gray)
K-->L{apakah Klasifikasi cascade terbaca ?}
L-->|ya|M(detect scale)
L-->|tidak|N(tampilkan frame)
M-->|Klik C|O(screenshoot)
O-->E
E-->P(simpan berdasarkan nama)
P-->D
D-->Q(kompresi image 50%)
Q-->F
F-->|hapus|P
N-->|klik q|R(quit)
```
![link youtube penjelasan](https://www.youtube.com/watch?v=3-BtwtwxLbI&feature=youtu.be).
# No 3. Mampu menjelaskan dan mendemonstrasikan aspek kecerdasan buatan dalam produk image processing yang dibuat
 aspek kecerdasan buatan pada code ini dapat di lihat dari library yang digunakan terutama CV2 yang di gunakan untuk image processing haarcascade ini yang di sediakan dengan dataset nya juga digunakan untuk mengolah image yang dimana pada akhirnya si camera dapat membaca dan mendeteksi apakah ada wajah atau tidak dalam hasil tangkapannya
![link folder](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/tree/main/face%20detection)
# No 4. Mampu mendemonstrasikan penggunaan teknologi audio processing dan audio compression dalam bentuk aplikasi sederhana.
![audio processing dan audio compression](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/raw/main/audio_processing_and_audio_compression.gif)
# No 5. Mampu menjelaskan cara kerja audio processing dan audio compression pada aplikasi yang dibuat.
```mermaid
flowchart TD
A[import library] --> B[pydub]
A --> C[os]
A --> D[Speech_recognition]
A --> E[googletrans]
A --> F[gtts]
A-->G[playsound]
A-->H[io]
A-->I[subprocess]
B-->J(input file)
J-->K(konversi menjadi.wav)
K-->D
D-->L(ubah audio menjadi teks)
L-->E
E-->M(translate teks ke bahasa lain)
M-->F
F-->N(konversi teks ke audio)
N-->H
H-->G
H-->O(konversi ke byte)
C-->O
O-->P(ubah byte ke audio kembali)
P-->Q{apakah file mau disimpan}
Q-->|ya|I-->R(simpan)
Q-->|Tidak|S(Hapus)
```
# No 6. Mampu menjelaskan dan mendemonstrasikan aspek kecerdasan buatan dalam produk audio processing yang dibuat
aspek kecerdasan buatan pada produk ini ialah dimana library speech_recognition ini melakukan proses pemecahan dan menganalisa audio ini kedalam format yang dapat di akses oleh engine speech_recognition yang diolah menggunakan neural network. sehingga audio yang di upload dapat di konversi menjadi teks
![link folder audio](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/tree/main/sound)

![hasil size compression](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/blob/main/compression_size.gif) 



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Lastwolf666/praktikum-multimedia.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Lastwolf666/praktikum-multimedia/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
