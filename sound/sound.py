from pydub import AudioSegment
import os
import speech_recognition as sr
from googletrans import Translator
from gtts import gTTS
from playsound import playsound
from io import BytesIO
import subprocess as sp


directory = '/home/smartcapt/Documents/Multimedia/sound/'

audio_file = input("Nama file audio: ")

output_file = input("Nama file output: ")

sound = AudioSegment.from_file(os.path.join(directory, audio_file), format="m4a")
sound.export(os.path.join(directory, output_file), format="wav")

r = sr.Recognizer()

with sr.AudioFile(os.path.join(directory, output_file)) as source:
    audio = r.record(source)

try:
    text = r.recognize_google(audio, language='id-ID')
    print("Hasil transkripsi: " + text)
except sr.UnknownValueError:
    print("Google Speech Recognition tidak dapat mengenali audio.")

target_lang = input("Masukkan kode bahasa target (misal: id untuk bahasa Indonesia): ")
translator = Translator()
translated_text = translator.translate(text, dest=target_lang).text

tts = gTTS(translated_text, lang=target_lang)
audio_bytes = BytesIO()
tts.write_to_fp(audio_bytes)

temp_file = os.path.join(directory, "temp.mp3")
with open(temp_file, "wb") as f:
    f.write(audio_bytes.getbuffer())

playsound(temp_file)

a = input("apakah anda mau menyimpan audio (y/n)")
if (a == 'y'):
    file_suara_hasil_tts_m4a_br_24 = os.path.join(directory, temp_file.split('.')[0] + '_br_24.m4a')
    result = sp.run(
        "ffmpeg -y -i " + os.path.join(directory, temp_file) + " -map 0:a:0 -b:a 24k -c:a aac -vn " + file_suara_hasil_tts_m4a_br_24,
        shell=True,
    )
else:
    os.remove(temp_file)
