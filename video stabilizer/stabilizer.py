import cv2
import numpy as np
from datetime import datetime

# Input nama file video
nama_file_video = input("Masukkan nama file video: ")

# Buka video
video = cv2.VideoCapture(nama_file_video)

# Mendapatkan properti video
fps = video.get(cv2.CAP_PROP_FPS)
frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Inisialisasi variabel
prev_frame = None
stabilized_video = None
prev_mean_shift = None

# Inisialisasi VideoWriter
current_time = datetime.now().strftime("%Y%m%d%H%M%S")
output_filename = f"video_stabilized_{current_time}.mp4"
fourcc = cv2.VideoWriter_fourcc(*"mp4v")  # Menggunakan codec MP4
output_video = cv2.VideoWriter(output_filename, fourcc, fps, (frame_width, frame_height), True)

while True:
    # Baca frame dari video
    ret, frame = video.read()
    if not ret:
        break
    
    # Ubah frame menjadi grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    # Proses stabilisasi pada frame pertama
    if prev_frame is None:
        prev_frame = gray
        stabilized_video = frame
        continue
    
    # Hitung optical flow antara frame sekarang dan sebelumnya
    optical_flow = cv2.calcOpticalFlowFarneback(prev_frame, gray, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    
    # Hitung pergeseran rata-rata dari optical flow
    mean_shift = optical_flow.mean(axis=0).mean(axis=0)
    
    # Terapkan moving average pada pergeseran rata-rata
    if prev_mean_shift is not None:
        alpha = 0.5  # Koefisien moving average
        mean_shift = alpha * mean_shift + (1 - alpha) * prev_mean_shift
    
    # Buat matriks transformasi untuk stabilisasi
    transform = np.float32([[1, 0, -mean_shift[0]], [0, 1, -mean_shift[1]]])
    
    # Stabilisasi frame menggunakan matriks transformasi
    stabilized_frame = cv2.warpAffine(frame, transform, (frame.shape[1], frame.shape[0]))
    
    # Tambahkan frame stabil ke video hasil
    stabilized_video = np.vstack((stabilized_video, stabilized_frame))
    
    # Tampilkan frame asli dan hasil stabilisasi
    cv2.imshow("Frame Asli", frame)
    cv2.imshow("Hasil Stabilisasi", stabilized_frame)
    
    # Tulis frame hasil stabilisasi ke video
    output_video.write(stabilized_frame)
    
    # Tombol 'q' untuk keluar
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
    # Update frame sebelumnya dan pergeseran rata-rata sebelumnya
    prev_frame = gray
    prev_mean_shift = mean_shift

# Tutup video dan jendela tampilan
video.release()
cv2.destroyAllWindows()

# Tutup video hasil
output_video.release()
